package br.com.animalsgame.domain;

import static br.com.animalsgame.domain.PoolState.ASKING;
import br.com.animalsgame.domain.tree.Animal;
import br.com.animalsgame.domain.tree.Characteristic;
import br.com.animalsgame.domain.tree.Node;

public class Pool {

	private Node rootNode;
	private Node currentNode;
	private PoolState state;
	private Boolean lastAnswer;

	public void init(Node initialNode) {

		this.rootNode = initialNode;
		this.reset();
	}

	public void reset() {

		this.currentNode = rootNode;
		this.state = ASKING;
		this.lastAnswer = null;
	}

	public Node getCurrentNode() {

		return this.currentNode;
	}

	public void setCurrentNode(Node currentNode) {

		this.currentNode = currentNode;
	}

	public void askQuestion() {

		System.out.format("%s (S/N): ", getCurrentNode().getQuestion());
	}

	public void enterAnswer(boolean answer) {

		getCurrentNode().evaluateAnswer(this, answer);
	}

	public void setNextNode(boolean answer) {

		Node current = getCurrentNode();
		this.setCurrentNode(answer ? current.getAffirmative() : current.getNegative());
	}

	public PoolState getState() {

		return this.state;
	}

	public void addNewAnimal(Animal newAnimal, Characteristic newCharacteristic) {

		Node current = getCurrentNode();
		Node parent = current.getParent();

		if (this.lastAnswer) {
			parent.setAffirmative(newCharacteristic);
		} else {
			parent.setNegative(newCharacteristic);
		}

		newCharacteristic.setParent(parent);
		newCharacteristic.setAffirmative(newAnimal);
		newCharacteristic.setNegative(current);

		this.state = ASKING;
	}

	public void setLastAnswer(boolean answer) {

		this.lastAnswer = answer;
	}

	public void setState(PoolState newState) {

		this.state = newState;
	}

}
