package br.com.animalsgame.domain;

public enum PoolState {

	ASKING, GUESSED, ADDING_NEW_ANIMAL;

}
