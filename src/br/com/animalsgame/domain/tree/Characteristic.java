package br.com.animalsgame.domain.tree;

import br.com.animalsgame.domain.Pool;

public class Characteristic extends Node {

	private String description;

	public Characteristic(String description) {

		this.description = description;
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	@Override
	public String getQuestion() {

		return String.format("Esse animal %s?", this.getDescription());
	}

	@Override
	public void evaluateAnswer(Pool pool, boolean answer) {

		pool.setNextNode(answer);
		pool.setLastAnswer(answer);
	}

}
