package br.com.animalsgame.domain.tree;

import static br.com.animalsgame.domain.PoolState.ADDING_NEW_ANIMAL;
import static br.com.animalsgame.domain.PoolState.GUESSED;
import br.com.animalsgame.domain.Pool;

public class Animal extends Node {

	private String name;

	public Animal(String name) {

		this.name = name;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	@Override
	public String getQuestion() {

		return String.format("Esse animal é um(a) %s?", this.getName());
	}

	@Override
	public void evaluateAnswer(Pool pool, boolean answer) {

		pool.setState(answer ? GUESSED : ADDING_NEW_ANIMAL);
	}

}
