package br.com.animalsgame.domain.tree;

import br.com.animalsgame.domain.Pool;

public abstract class Node {

	private Node parent;
	private Node affirmative;
	private Node negative;

	public abstract String getQuestion();

	public abstract void evaluateAnswer(Pool pool, boolean answer);

	public void setAffirmative(Node node) {

		node.setParent(this);
		this.affirmative = node;
	}

	public void setNegative(Node node) {

		node.setParent(this);
		this.negative = node;
	}

	public void setParent(Node node) {

		this.parent = node;
	}

	public Node getNegative() {

		return this.negative;
	}

	public Node getAffirmative() {

		return this.affirmative;
	}

	public Node getParent() {

		return this.parent;
	}

}
