package br.com.animalsgame;

import java.util.Scanner;

import br.com.animalsgame.domain.Pool;
import br.com.animalsgame.domain.tree.Animal;
import br.com.animalsgame.domain.tree.Characteristic;
import br.com.animalsgame.domain.tree.Node;

public class AnimalsGame {
	private Scanner scanner;

	public static void main(String[] args) {

		AnimalsGame game = new AnimalsGame();
		Pool pool = game.setUpInitialPool();
		while (true) {

			game.start(pool);
		}
	}

	public AnimalsGame() {

		this.scanner = new Scanner(System.in);
	}

	private Pool setUpInitialPool() {

		Node livesInWater = new Characteristic("vive na água");
		Node shark = new Animal("tubarão");
		Node monkey = new Animal("macaco");

		livesInWater.setAffirmative(shark);
		livesInWater.setNegative(monkey);

		Pool pool = new Pool();
		pool.init(livesInWater);
		return pool;
	}

	private void start(Pool pool) {

		pool.reset();

		System.out.println("Pense em um animal...");
		askQuestion(pool);
	}

	private void askQuestion(Pool pool) {

		pool.askQuestion();
		pool.enterAnswer(getAnswer());

		switch (pool.getState()) {
		case GUESSED:
			System.out.println("Adivinhei de novo!");
			break;
		case ADDING_NEW_ANIMAL:
			addNewAnimal(pool);
			break;
		case ASKING:
			askQuestion(pool);
			break;
		default:
			throw new IllegalStateException("ERROR! PoolState not implemented.");
		}
	}

	private void addNewAnimal(Pool pool) {

		System.out.println("Qual animal você pensou?");
		String animalName = scanner.nextLine();
		Animal currentAnimal = (Animal) pool.getCurrentNode();
		System.out.format("Um(a) %s ______, mas um %s não.\n", animalName, currentAnimal.getName());
		String animalCharacteristic = scanner.nextLine();

		Animal newAnimal = new Animal(animalName);
		Characteristic newCharacteristic = new Characteristic(animalCharacteristic);
		pool.addNewAnimal(newAnimal, newCharacteristic);
	}

	private boolean getAnswer() {

		boolean answer = false;

		String input = scanner.nextLine();
		if ("S".equalsIgnoreCase(input)) {
			answer = true;
		}

		return answer;
	}

}
